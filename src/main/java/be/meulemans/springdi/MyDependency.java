package be.meulemans.springdi;

public interface MyDependency {

    String generate();
}
