package be.meulemans.springdi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FieldInjectionService {

    @Autowired
    private MyDependency myDependency;

    public String foo() {
        return myDependency.generate() + " thanks field!";
    }

}
