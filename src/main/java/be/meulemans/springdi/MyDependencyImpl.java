package be.meulemans.springdi;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class MyDependencyImpl implements MyDependency {

    @Override
    public String generate() {
        return "generated: " + UUID.randomUUID().toString();
    }
}
