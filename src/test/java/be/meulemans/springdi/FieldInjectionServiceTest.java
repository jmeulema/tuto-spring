package be.meulemans.springdi;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class FieldInjectionServiceTest {

    @Autowired
    private FieldInjectionService fieldInjectionService;

    @Test
    public void foo() {
        Assertions.assertThat(fieldInjectionService.foo())
                .contains("field")
                .contains("generated");
    }

}
