package be.meulemans.springdi;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ConstructorInjectionServiceTest {

    @Autowired
    private ConstructorInjectionService constructorInjectionService;

    @Test
    public void foo() {
        Assertions.assertThat(constructorInjectionService.foo())
                .contains("constructor")
                .contains("generated");
    }
}